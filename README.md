# httpserver.js

Um servidor de HTTP que pode ser utilizado para gerar um controle de IoT (Internet da Coisas) em JavaScript junto com o NODEJS.

----

1º - Digite em terminal:

 * $ ```` nodejs ./httpserver.js````

2º - Acesse o site pelo seu navegador:

 * http://localhost:8080/hello_world.html?year=2017&month=february
