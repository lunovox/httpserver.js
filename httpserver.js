/*
Digite em terminal:
	$ nodejs ./httpserver.js
*/

var http = require('http');
const url = require('url');
const port = 8080;
const ip = 'localhost';

//create a server object:
http.createServer(function (req, res) {
	console.log("------------------------------------------------------");

	res.writeHead(200, {'Content-Type': 'text/html'});

	var myURL = url.parse(req.url, true);

	//returns 'localhost:8080'
	console.log("http://"+ip+":"+port+req.url);
	res.write("<h1><b>http://"+ip+":"+port+req.url+"</b></h1><br/>\n");

	//returns '/default.htm'
	console.log("myURL.pathname="+myURL.pathname); 
	res.write("myURL.pathname="+myURL.pathname+"<br/>\n");

	//returns '?year=2017&month=february'
	console.log("myURL.search="+myURL.search); 
	res.write("myURL.search="+myURL.search+"<br/>\n");

	//returns an object: { year: 2017, month: 'february' }
	console.log("myURL.query.year="+myURL.query.year); 
	res.write("myURL.query.year="+myURL.query.year+"<br/>\n");
	console.log("myURL.query.month="+myURL.query.month); 
	res.write("myURL.query.month="+myURL.query.month+"<br/>\n");
	console.log("myURL.query.exit="+myURL.query.exit); 
	res.write("myURL.query.exit="+myURL.query.exit+"<br/>\n");
	
	res.write("<hr/>Clique <a href='http://"+ip+":"+port+"/?exit'><b>aqui</b></a> para fechar este servidor!");

	if(myURL.query.exit!=undefined){
		console.log("------------------------------------------------------");
		console.log("Servidor fechado!"); 
		res.write("<hr/>Servidor fechado!<br/>\n");
		return this.close();
	}

	res.end();
}).listen(port, ip); //the server object listens on port 8080 

console.log("Acesse o site:");
console.log(" * http://"+ip+":"+port+"/hello_world.html?year=2017&month=february\n");
console.log("Para fechar o servidor acesse:");
console.log(" * http://"+ip+":"+port+"/?exit\n");
